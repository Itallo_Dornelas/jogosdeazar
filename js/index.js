let pedra = 0;
let tesoura = 1;
let papel = 2;

const getOutputString = (numberPlay) => {
  let choice = "Pedra";
  if (numberPlay === papel) {
    choice = "Papel";
  }
  if (numberPlay === tesoura) {
    choice = "Tesoura";
  }
  return choice;
};

const getChoiceJonyMan = () => {
  let choiceJonyMan = pedra;
  let choiceNumbers = Math.floor(Math.random() * 3);
  if (choiceNumbers === papel) {
    choiceJonyMan = papel;
  }
  if (choiceNumbers === tesoura) {
    choiceJonyMan = tesoura;
  }
  return choiceJonyMan;
};

const buttonChoices = document.getElementById("option");
buttonChoices.addEventListener("click", function (evt) {
  if (evt.target.tagName === "I") {
    const choiceSelect = evt.target;
    const choicePlayer = Number(choiceSelect.dataset.escolha);
    const playerChoice = getOutputString(choicePlayer);
    let jonyManPlay = getChoiceJonyMan();
    const jonyManChoice = getOutputString(jonyManPlay);
    jogadaResutaldo(` ${playerChoice} VS ${jonyManChoice} `);
    win(choicePlayer, jonyManPlay);
  }
});
const jogadaResutaldo = (mensagen) => {
  document.getElementById("resultadoEscolha").innerHTML = mensagen;
};
