function User() {
  const nome = document.getElementById("nome").value;
  const pla = document.getElementById("player");
  pla.innerText = `${nome}`;
}

function changePage() {
  const pg1 = document.getElementById("pg1");
  const pg2 = document.getElementById("pg2");
  pg1.disabled = true;
  pg2.disabled = false;
  document.querySelector("#change").style.display = " none";
  document.querySelector("#form").style.display = " none";
  document.querySelector("#title").style.display = " none";
  document.querySelector("#movsIcon").style.display = " none";

  User();
}
const change = document.getElementById("change");
change.addEventListener("click", changePage);
