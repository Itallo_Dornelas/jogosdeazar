const win = (choicePlayer, jonyManPlay) => {
  const playerPontos = document.getElementById("player-pontos");
  const jonyPontos = document.getElementById("jonyman-pontos");
  let win = false;
  let pontos = jonyPontos;

  if (choicePlayer !== jonyManPlay) {
    if (choicePlayer === pedra && jonyManPlay === tesoura) {
      win = true;
    }
    if (choicePlayer === tesoura && jonyManPlay === papel) {
      win = true;
    }
    if (choicePlayer === papel && jonyManPlay === pedra) {
      win = true;
    }
    if (win) {
      pontos = playerPontos;
      let placar = Number(pontos.dataset.pontosAtuais);
      placar++;
      pontos.dataset.pontosAtuais = placar;
      pontos.innerText = placar.toString();
      popUpWin("Voce Ganhou!");
    } else if (!win) {
      let placar = Number(pontos.dataset.pontosAtuais);
      placar++;
      pontos.dataset.pontosAtuais = placar;
      pontos.innerText = placar.toString();
      popUpWin("Voce Perdeu!");
    }
  } else {
    popUpDraw("Empatou!");
  }
};
const popUpDraw = (mensagen) => {
  document.getElementById("popUpWin").innerHTML = mensagen;
  document.getElementById("popUp").style.display = "block";
  document.getElementById("popUpWin").style.display = "block";
  document.getElementById("glass").style.display = "block";
};
const popUpWin = (mensagen) => {
  document.getElementById("popUpWin").innerHTML = mensagen;
  document.getElementById("popUp").style.display = "block";
  document.getElementById("popUpWin").style.display = "block";
  document.getElementById("glass").style.display = "block";
};

const buttonRestart = () => {
  restartGame();
  document.getElementById("popUp").style.display = "none";
  document.getElementById("glass").style.display = "none";
  document.getElementById("popUpWin").style.display = "none";
};
const buttonReset = document.getElementById("restartButton");
buttonReset.addEventListener("click", buttonRestart);
const restartGame = () => {
  let buttonChoices = document.getElementById("option");
  buttonChoices = innerHTML = "";
};
